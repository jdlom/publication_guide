# [coord] Quelles questions se poser sur la gestion des données ? {#questions-donnees}

- Allez-vous utiliser des données dans votre projet ?
- Sont-elles accessibles ?
- Sont-elles publiques ?
- Sous quel format sont-elles présentées ?
- À quelle fréquence sont elles mises à jour ?
- Qui sont les créateurs de ces données ?
  + Font-ils partie du projet ?
  + Si non, ce serait bien de réussir à les intégrer de temps en temps
- Quelle est la taille des données ?
  + Quelle est la vraie taille des données, en espace disque ? 
  + Ces données peuvent-elles entrer en entier sur le dépôt _git_, potentiellement libre et accessible à tous ?
- Votre projet nécessite-t-il le stockage des données sur des machines utilisateurs (Création d'un livre numérique en local) ?
  + Vous aurez besoin de documenter comment les récupérer, les installer et y accéder
- Votre projet nécessite-t-il un accès pour un serveur web (Mise en ligne d'une application Shiny) ?
- Les données peuvent-elles être stockées dans une base de données SQL ?
  + La base est-elle accessible aux utilisateurs du projet ?
  + La base est-elle accessible aux serveurs de développement ?
  + Avec quelles protections ?

Selon les cas, plusieurs méthodes de stockage et de communication avec R sont possibles.

- [{rappdirs}](https://rappdirs.r-lib.org/) permet de stocker des données en local sur les postes des utilisateurs.
- [{pins}](https://pins.rstudio.com/index.html) permet de faciliter la mise à jour et l'exploitation de fichiers plats externes.
- [{duckdb}](https://duckdb.org/docs/api/r) permet d'embarquer une base de donnée.
- [{DBI} et autres bases de données](https://db.rstudio.com/) permet de se connecter à des bases de données existantes.

Cet article peut intéresser les [Dev] : [How to distribute data with your R package](https://blog.r-hub.io/2020/05/29/distribute-data/)

# [dev] Release : Validation - Livraison par les [ChfDev] {#validation-livraison}

Lorsque les [ChfDev] sont satisfaits des avancées et souhaitent passer en phase de validation, une nouvelle version voit le jour.
Plusieurs versions intermédiaires peuvent voir le jour avant la version finale espérée.
Ces phases intermédiaires permettent de fermer des tickets, et donc de libérer la colonne "A valider" du Kanban.

## Étapes avant livraison

- Informer les [Dev] que les développements sont figés
- Figer les développements de la branche "dev"
- S'assurer que les consignes d'utilisation du projet sont partagées avec les [Ed]. Ils s'appuieront sur ce guide pour leurs tests.
- S'assurer que les `check()` passent, sur le CI et en local
- Créer une branche de "release"
-   Augmenter le numéro de version du package
-   Compléter et compiler le fichier "README.Rmd"
    -   Pour lister les dépendances, on peut s'aider de `chameleon::create_pkg_desc_file(to = "markdown")`
-   Organiser le contenu du fichier "NEWS.md"
- S'assurer que les `check()` passent en local
-   Envoyer sur le serveur Git
- Intégrer à la branche "dev"
- S'assurer que les `check()` passent, sur le CI
-   Sur GitLab/GitHub, créer un *tag* sur le *commit* avec le numéro de version du package
    - Exemple `v0.1.0`
    - La description du tag peut être une version allégée du fichier NEWS.md
- S'assurer que tous les tickets réglés pour cette version ont été déplacés dans la colonne [**A valider**]{style="color: #D10069"} du Kanban

<!-- ## Phase de validation -->
```{r, child="sections/phase-validation.Rmd"}
```

## Choisir un numéro de version pour la livraison

Un numéro de version se présente en trois morceaux : `majeur.mineur.patch`.
Généralement, les versions intermédiaires qui permettent de valider un grand nombre de ticket entrent dans la catégorie `mineur`. 
Lors de la phase de validation par les [Ed], il pourra y avoir des modifications, corrections de dernière minute à ajouter. 
Ces corrections feront monter le niveau de `patch`.
Répéter les étapes avant livraison et la phase de validation, augmenter le niveau de `patch` à chaque livraison, jusqu'à validation de tous les tickets prévus.

## Livraison

Lorsque tous les tickets prévus ont été validés et que les ouvertures de nouveaux tickets ne présentent plus que des nouvelles demandes, la version peut être livrée. 
Selon l'avancement de votre projet, ce n'est peut-être pas une livraison finale, ni 100% utilisable, mais elle permet d'avoir un point d'étape rassurant et encourageant pour la suite.

-   Faire une *merge* du "master" vers la branche "production"
- Communiquez avec tous les membres du projet pour fêter ça !
- Faites une réunion de projet pour avoir les retours de chacun sur comment s'est passée cette phase de validation
  + Un échange préalable sur le logiciel de Chat en asynchrone, avec un quizz par exemple, permet de savoir si les participants sont plutôt satisfaits, fatigués, heureux, ...
  + Un pad collaboratif mis en place en amont permet à chacun de lister:
    + Moteur : Ce qui vous a motivé / propulsé lors de cette phase
    + Ralentisseur : Ce qui vous a ralentit
    + Obstacle : Ce qui vous a bloqué, a été dur a gérer
    + Aide : Ce qui vous a aidé à avancer
    + Amélioration : Ce qui pourrait être amélioré pour la prochaine fois
    
> On entre dans un nouvelle phase de développement : Prenez le bonus de tour et retournez à la case ["priorisation et répartition des tâches"](#gestion-des-issues-avec-le-kanban)...


## Documentation de la livraison

Votre projet ne se limite pas aux publications qui vont en découler.
Toute la documentation et les tests peuvent être mis en avant pour montrer que votre travail a tous les atouts pour être reproduit, maintenu et étendu.  

Sur le README et dans vos communications, vous pouvez lister les différents liens et documents suivants:

- Lieu de stockage des comptes-rendus de réunions
- Lien vers le dépôt Git du projet
- Lien vers le site {pkgdown} créé automatiquement lors de l'intégration continue
  + Pensez qu'il peut être amendé d'une page présentant les résultats des tests unitaires avec [{covrpage}](https://gihub.com/metrumresearchgroup/covrpage)
- Lien vers une page de présentation des tests unitaires détaillés avec [{testdown}](https://github.com/ThinkR-open/testdown)
- Lien vers une page de présentation des messages de commit par ticket avec [{gitdown} ](https://thinkr-open.github.io/gitdown/)
  + _cf._ https://rtask.thinkr.fr/fr/telecharger-les-issues-gitlab-ou-github-et-faire-un-rapport-resume-de-vos-commits/
- Lien vers les diaporamas et autres documents de communication sur le projet
- Lien vers les productions


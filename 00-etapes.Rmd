# Étapes du projet dans l'ordre chronologique

## Mise en place

- [coord] - Définition des rôles de coordination des équipes et du calendrier : ["Comment coordonner le projet ?"](#coordonner-projet)
- [coord] - Mettre en place les outils de communication : ["Les outils de travail"](#coordonner-outils)
- [edition] - Discussion sur les besoins et un contenu générique : ["Comment définir ses besoins et le contenu"](#lister-besoins)
- [coord] - Définition du format et du stockage des données : ["Quelles questions se poser sur la gestion des données ?"](#questions-donnees)
- [dev] - Mettre en place le projet de développement en lien avec _git_, RStudio et GitLab : ["Mettre en place un projet de développement avec RStudio et GitLab"](#dev-gerer-gitlab)

## Le cycle de développement *DevOps*

Chaque demande du comité éditorial sera listée dans un ou plusieurs tickets. 
Chaque développeur qui prend en charge un ticket fera valider ses développements par un responsable du développement, puis le résultat par le comité éditorial avant de pouvoir fermer ce ticket. 
D'une manière générale, pour une production avec R, la validation concerne (1) l'apparence générale de la production (hors contenu pertinent), (2) le contenu (non mis en forme), et (3) le contenu intégré dans le moyen de production.
Dans le cadre d'une publication PROPRE, la validation concerne les outils d'aide à la création de contenu.  

Le produit de développement est toujours un **_package_ R, documenté, testé et versionné**. 
Le produit final visé est une publication reproductible créée à partir de ce *package*.
Les étapes de mise en place doivent avoir permis de séparer la publication en différents chapitres, si possible indépendants.
La publication sera construite en plusieurs étapes, répétées pour chaque chapitre.

1. *UI first* : Création et validation de l'apparence du futur produit (*bookdown*, sans contenu) et de la façon de le construire par les utilisateurs (Comment apparaît le menu de création de la publication).

2. *Rmd then* : 
    + [dev] : Création des vignettes du package. Plutôt dédiées à la documentation du développement, elles présentent l'utilisation directe des fonctions créées. Les vignettes compilées seront présentées sur un site web pour validation du contenu et des traitements des données par le comité éditorial [Ed]. 
    + [edition] : Création du contenu des textes de la publication. Les titres et paragraphes sont validés en comité éditorial. En théorie, il ne devrait pas y avoir de discussion sur la structure du chapitre à ce stade.

3. *Documentation* utilisateurs. C'est une extension de la partie *Rmd then* qui doit expliquer aux utilisateurs du produit comment l'utiliser. 

4. *Integration* du contenu dans le produit. Lors de cette phase, il ne devrait plus y avoir de discussion ni sur l'apparence, ni sur le contenu, ni sur le fond. Les codes R sont figés. Il s'agit surtout de vérifier que tout se passe bien dans le produit final.

Les 1., 2. et 3. peuvent se faire indépendamment, en parallèle.
Le 4. se fait à la fin. Ce peut être la fin de la validation d'un chapitre en particulier. Mais pas d'un paragraphe.  

Les guides nécessaires à la bonne réalisation des cycles de développements sont les suivants :

- [dev] - Créer les tickets du projet dans le Kanban et les utiliser comme la référence pour le développement : ["Créer des tickets et gérer la communication et l'avancement avec un Kanban"](#tickets-kanban)
- [edition] - Voir l'avancée de la résolution des tickets et répondre aux interrogations des développeurs : [Suivre le traitement de ses besoins](#suivi-besoins)
- [dev] - Développer en utilisant _git_, GitLab et RStudio comme outils de collaboration : ["Collaboration sur un projet Git avec Gitlab et Rstudio"](#collaborer-git)
- [dev] - Développer un package documenté et testé avec la méthode 'Rmd first'. Cette méthode est valable pour les étapes de développement ci-dessus : ["Développer un package documenté et testé avec la méthode 'Rmd first'](#dev-rmd-first)
- [dev] - Garder en tête les ["Pratiques, trucs et astuces de code avec R"](#pratiques-astuces)
- [edition] Participer à la rédaction des textes à insérer dans le produit final : ["Rédiger et fournir les textes en Rmarkdown"](#rediger-textes)

## Tests utilisateurs et livraisons

À l'issue d'un cycle, le contenu est figé, les développements de nouvelles fonctionnalités sont figés. 
On entre dans une phase de validation. 
Les équipes de développement et d'édition testent le bon fonctionnement des outils

- [dev] Tester et livrer : ["Release : Validation - Livraison par les [ChfDev]"](#validation-livraison)
- [edition] ["Tester, reporter les bugs, valider"](#tester-reporter)
- [edition] ["Produire, diffuser et communiquer"](#diffuser-communiquer)



